<?php
add_action( 'admin_menu', 'MOHO_option_page_add_menu' );
function MOHO_option_page_add_menu() {
	add_menu_page(
		'MOHO_option', // page <title>Title</title>
		'Administrador de Modal', // menu link text
		'manage_options', // capability to access the page
		'MOHO_option-slug', // page URL slug
		'MOHO_function_option_page', // callback function /w content
		'dashicons-grid-view', // menu icon
		5 // priority
	);
 
}
add_action( 'admin_init',  'MOHO_option_page_register_setting' );
 
function MOHO_option_page_register_setting(){
 
	register_setting(
		'MOHO_option_settings', // settings group name
		'input_MOHO_option_settings', // option name
		'sanitize_text_field' // sanitization function
	);
	add_settings_section(
		'MOHO_option_settings_section_id', // section ID
		'', // title (if needed)
		'', // callback function (if needed)
		'MOHO_option-slug' // page slug
	);
 
	add_settings_field(
		'input_MOHO_option_settings',
		'input_MOHO_option_settings',
		'MOHO_option_text_field_html', // function which prints the field
		'MOHO_option-slug', // page slug
		'MOHO_option_settings_section_id', // section ID
		array( 
			'label_for' => 'input_MOHO_option_settings',
			'class' => 'misha-class', // for <tr> element
		)
    );
}
function MOHO_option_text_field_html(){
 
	$text = get_option( 'input_MOHO_option_settings' );
 
	printf(
		'<input type="text" id="input_MOHO_option_settings" name="input_MOHO_option_settings" value="%s" />',
		esc_attr( $text )
	);
 
}
function MOHO_function_option_page(){
    $options = get_option( 'input_MOHO_option_settings' );
    if($options == "")
        $options = "{}";
    $options = json_decode($options,true);
    ?> 
    <div class="wrap">
        <h1>
            Aministrador de Modal
        </h1>
        <table>
            <tr>
                <th colspan="2">
                    <h2>
                        Configuracion de Modal
                    </h2>
                </th>
            </tr>
            <tr>
                <th>
                    <label for="active">
                        Activar Modal
                    </label>
                </th>
                <td>
                    <input
                    id="active"
                    name="active"
                    type="checkbox"
                    <?=($options['active']=="yes")?"checked":""?>
                    />
                </td>
            </tr>
            <tr>
                <th>
                    <label for="title">
                        Titulos
                    </label>
                </th>
                <td>
                    <input
                    id="title"
                    name="title"
                    placeholder="Title"
                    type="text"
                    value="<?=$options['title']?>"
                    />
                </td>
            </tr>
            <tr>
                <th>
                    <label for="text">
                        Texto
                    </label>
                </th>
                <td>
                    <input
                    id="text"
                    name="text"
                    placeholder="Texto"
                    type="text"
                    value="<?=$options['text']?>"
                    />
                </td>
            </tr>
            <tr>
                <th>
                    <label for="btn">
                        Boton
                    </label>
                </th>
                <td>
                    <input
                    id="btn"
                    name="btn"
                    placeholder="Texto del Boton"
                    type="text"
                    value="<?=$options['btn']?>"
                    />
                </td>
            </tr>
            <tr>
                <th colspan="2">
                    <h2>
                        Configuracion de Email
                    </h2>
                </th>
            </tr>
            <tr>
                <th>
                    <label for="email">
                        Email
                    </label>
                </th>
                <td>
                    <input
                    id="email"
                    name="email"
                    placeholder="Email"
                    type="text"
                    value="<?=$options['email']?>"
                    />
                </td>
            </tr>
            <tr>
                <th>
                    <label for="subject">
                        Subject
                    </label>
                </th>
                <td>
                    <input
                    id="subject"
                    name="subject"
                    placeholder="Subject"
                    type="text"
                    value="<?=$options['subject']?>"
                    />
                </td>
            </tr>
            <tr>
                <th>
                </th>
                <td>
                    <label
                    class="button button-primary"
                    for="ejmModal">
                        Ver Ejemplo de modal
                    </label>
                    <input
                    id="ejmModal"
                    name="ejmModal"
                    type="checkbox"
                    />
                    <div class="contentEjmModal">
                        <?=do_shortcode("[MOHO_shortcode_modal]")?>
                    </div>
                </td>
            </tr>
        </table>
	    <form method="post" action="options.php">
            <?php
                settings_fields( 'MOHO_option_settings' ); 
                do_settings_sections( 'MOHO_option-slug' ); 
                submit_button();
            ?>
	    </form>
        <style>
            .misha-class,
            #ejmModal,
            .contentEjmModal{
                display:none;
            }
            #ejmModal:checked + .contentEjmModal{
                display:block
            }
        </style>
        <script>
            active = document.getElementById('active')
            title = document.getElementById('title')
            text = document.getElementById('text')
            btn = document.getElementById('btn')

            email = document.getElementById('email')
            subject = document.getElementById('subject')

            input = document.getElementById('input_MOHO_option_settings')


            submit = document.getElementById('submit')

            submit.onclick = () => {
                data = {}
                data.active = active.checked
                data.title = title.value
                data.text = text.value
                data.btn = btn.value

                data.email = email.value
                data.subject = subject.value

                input.value = JSON.stringify(data)
            }
        </script>
    </div>
    <?php
}