<?php
function MOHO_body_open()
{
    if(is_front_page()){
        echo do_shortcode("[MOHO_shortcode_modal]");
    }
}
add_action( 'wp_body_open', 'MOHO_body_open' );