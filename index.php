<?php
/*
Plugin Name: ModalHome
Plugin URI: https://startscoinc.com/es/
Description: Crea una modal que es necesaria completar par acceder
Author: Startsco
Version: 0.1
Author URI: https://startscoinc.com/es/#
License: 
*/
define("PREFMH","MOHO");
require_once plugin_dir_path( __FILE__ ) . 'pageOption.php';
require_once plugin_dir_path( __FILE__ ) . 'shortcode.php';
require_once plugin_dir_path( __FILE__ ) . 'sendEmail.php';
require_once plugin_dir_path( __FILE__ ) . 'hook.php';
