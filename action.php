<?php
require_once(preg_replace('/wp-content.*$/','',__DIR__).'wp-load.php');
$data = json_decode(file_get_contents('php://input'), true);
if(isset($data)){
    $_POST = $data;
}

$result = array(
    "status"    => "error",
    "message"       => ""
);
switch ($_POST["action"]) {
    case 'sendDataEmail':
        if(MOHO_sendTicketEmail($_POST["data"])){
            $result["status"] = "ok";
        }else{
            $result["message"] = "Error al enviar el correo";
        }
        break;
}
echo json_encode($result);