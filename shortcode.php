<?php
function MOHO_shortcode_modal( $atributos, $content = null ) {
    $options = get_option( 'input_MOHO_option_settings' );
    if($options == "")
        $options = "{}";
    $options = json_decode($options,true);

    if(!($options['active']=="yes"))
        return "";

	$atributos = shortcode_atts( array(
		'nombre'  => '',
		'apellido'   => '',	
		'email'   => '',	
		'celular'   => '',
		'active'   => 'active',		
	), $atributos, 'atributes' );

    $arrow = '<img alt="arrow" src="'.plugin_dir_url( __FILE__ ).'/img/arrow.svg'.'" />';

    ob_start();
    ?>
    <div id="MOHO_modal" class="content_modal <?=$atributos['active']?>">
        <div class="modal">
            <h3 class="header">
                <?=$options['title']?>
            </h3>
            <div class="body">
                <div class="arrow arrowtop"><?=$arrow?></div>
                <div class="arrow arrowbottom"><?=$arrow?></div>
                <div class="content">
                    <p class="text">
                        <?=$options['text']?>
                    </p>
                    <div class="form">
                        <label for="MOHO_nombre">
                            <span>nombre</span>
                            <input 
                            id="MOHO_nombre"
                            name="nombre"
                            type="text"
                            value="<?=$atributos['nombre']?>"
                            >
                        </label>
                        <label for="MOHO_apellido">
                            <span>apellido</span>
                            <input 
                            id="MOHO_apellido"
                            name="apellido"
                            type="text"
                            value="<?=$atributos['apellido']?>"
                            >
                        </label>
                        <label for="MOHO_email">
                            <span>email</span>
                            <input 
                            id="MOHO_email"
                            name="email"
                            type="email"
                            value="<?=$atributos['email']?>"
                            >
                        </label>
                        <label for="MOHO_celular">
                            <span>celular</span>
                            <input 
                            id="MOHO_celular"
                            name="celular"
                            type="tel"
                            value="<?=$atributos['celular']?>"
                            >
                        </label>
                    </div>
                    <p class="text">
                        <button id="MOHO_btn_modal">
                            <?=$options['btn']?>
                        </button>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <style>
        .content_modal,.content_modal *{
            box-sizing: border-box;
        }
        .content_modal{
            position:fixed;
            width:100%;
            height:100%;
            overflow:auto;
            background:#000;
            z-index:99999999;
            display:flex;
            justify-content:center;
            padding:10px;
            top: 0;
            left: 0;
        }
        .content_modal:not(.active){
            display:none;
        }
        .content_modal.load:before{
            content:"";
            display:block;
            width:150px;
            height:150px;
            border-radius:100%;
            border:10px solid #FFB612;
            border-top-color:transparent;
            animation:girarload 2s infinite;

            top:0;
            left:0;
            right:0;
            bottom:0;
            margin:auto;

            position:absolute;
            z-index: 9;
            transition: 1s;
        }
        @keyframes girarload {
            to{
                transform: rotateZ(360deg);
            }
        }
        .content_modal.load .modal{
            filter: brightness(0);
        }
        .content_modal .modal{
            width:100%;
            max-width:800px;
        }
        .content_modal .modal .header{
            background:#122444;
            color:#fff;
            text-align:center;
            padding:10px 20px; 
            font-size: 60px;
            display: block;
            margin:0;
            line-height: 1;
            text-weight:700px;
        }
        .content_modal .modal .body{
            background:#FFB612;
            display:flex;
            justify-content:center;
            padding:10px;
            position:relative;
        }
        .content_modal .modal .arrow{
            position:absolute;
            top:0;
            left:0;
            width:90px;
            height: auto;
            margin: 0;
            z-index:1;
            max-width:50%;
        }
        .content_modal .modal .arrow:after{
            display:none;
        }
        .content_modal .modal .arrowbottom{
            top:initial;
            left:initial;
            right:0;
            bottom:0;
            transform: rotateZ(180deg);

            width:160px;
        }
        .content_modal .modal .arrow img{
            width:100%;
            height:auto;
        }
        .content_modal .modal .content{
            width:100%;
            max-width:600px;
            position:relative;
            z-index:10;
        }
        .content_modal .modal .text{
            color:#fff;
            text-align:center;
            font-size: 33px;
            line-height: 1;
            margin: 0;
            margin-bottom: 10px;
        }
        .content_modal .modal label{
            display:block;
            width:100%;
            margin-bottom:10px;
        }
        .content_modal .modal label span{
            color:#122444;
            font-size:30px;
            text-transform:uppercase;
            font-weight:700;
            display:block;
            width:100%;
            line-height:1;  
            margin-bottom:5px; 
        }
        .content_modal .modal label input{
            display:block;
            width:100%;
            border:0;
            border-radius:25px;
            padding: 0 25px;
            font-size:25px;
            color:#122444;
        }
        .content_modal .modal button{
            background:#BE1E2D;
            color:#fff;
            font-size:35px;
            text-transform:uppercase;
            font-weight:700;
            line-height:1;  
            padding:15px 35px;
            border:0;
            cursor:pointer;
            margin:15px auto;
        }
    </style>
    <script>
        modal = document.getElementById('MOHO_modal')

        nombre = document.getElementById('MOHO_nombre')
        apellido = document.getElementById('MOHO_apellido')
        email = document.getElementById('MOHO_email')
        celular = document.getElementById('MOHO_celular')
        
        btn_modal = document.getElementById('MOHO_btn_modal')
        
        validateData = (data) => {
            var sw = false
            if(data.nombre == "" || data.nombre == null || data.nombre == undefined){
                alert("El Nombre es obligatorio")
            }else if(data.apellido == "" || data.apellido == null || data.apellido == undefined){
                alert("El Apellido es obligatorio")
            }else if(data.email == "" || data.email == null || data.email == undefined){
                alert("El Email es obligatorio")
            }else if(data.celular == "" || data.celular == null || data.celular == undefined){
                alert("El Celular es obligatorio")
            }else{
                emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
                if (!emailRegex.test(data.email )) {
                    alert("El Email es invalido")
                } else {
                    sw = true
                }
            }
            return sw
        }

        btn_modal.onclick = () => {
            data = {
                nombre : nombre.value,
                apellido : apellido.value,
                email : email.value,
                celular : celular.value,
            }
            if(validateData(data)){
                sendData(data)
            }
        }

        sendData = (data) => {
            modal.classList.add("load")
            var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");

            dataSend = {
                action : "sendDataEmail",
                data : data
            }

            var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: JSON.stringify(dataSend),
            redirect: 'follow'
            };

            fetch("<?=plugin_dir_url( __FILE__ )."action.php"?>", requestOptions)
                .then(response => response.text())
                .then(r => {
                    var result = JSON.parse(r)
                    console.log(r);
                    console.log(result);
                    if(result.status == "ok"){
                        console.log(result)
                        modal.classList.remove("active")
                        localStorage.sendForm = true
                    }else{
                        alert(result.message)
                    }
                    modal.classList.remove("load")
                })
                .catch(error => console.log('error', error));
        }
        validateStorage = () => {
            if(localStorage.sendForm == "true"){
                modal.classList.remove("active")
            }
        }
        validateStorage()
    </script>
    <?php
	return ob_get_clean();
}
add_shortcode('MOHO_shortcode_modal', 'MOHO_shortcode_modal');