<?php
function MOHO_sendTicketEmail($data)
{
    
    $options = get_option( 'input_MOHO_option_settings' );
    if($options == "")
        $options = "{}";
    $options = json_decode($options,true);


    $from = parse_url(get_site_url())["host"].' <'.$options['email'].'>';
    $subject = $options['subject'];
    $to = $options['email'];
    
    // To send HTML mail, the Content-type header must be set
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    
    $headers .= 'From: '.$from."\r\n".
            'Reply-To: '.$from."\r\n" .
            'X-Mailer: PHP/' . phpversion();

    ob_start();
    ?>
    <html>
        <body>
            <h1><?=_("Nuevo registro")?></h1>
            <?php
                foreach ($data as $key => $value) {
                    ?>
                    <p>
                        <strong>
                            <?=$key?>:
                        </strong>
                        <?=$value?>
                    </p>
                    <?php
                }
            ?>
        </body>
    </html>
    <?php
    $message = ob_get_clean();
    
    return wp_mail($to, $subject, $message, $headers);
}
